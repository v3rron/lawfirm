class Message < MailForm::Base
  attr_accessor :full_name, :email, :phone, :subject, :message

  validates :full_name, :email, :subject, :message, :presence => true
  validates :email, :format => { :with => %r{.+@.+\..+} }, :allow_blank => true

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end
end