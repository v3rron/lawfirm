# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  if document.documentElement.clientWidth < 769
    $('.navbar').addClass 'navbar-static-top'
  $('#contact-form').load 'contact#new', ->
    $('#nav-message').on 'click', (e) ->
      e.preventDefault()
      form = $('#contact-form')
      if form.is(':hidden')
        form.show 'slide', {direction: 'right'}, 'fast'
      else
        form.hide 'slide', {direction: 'right'}, 'fast'

    $('#contact-form .form-close').on 'click', (e) ->
      e.preventDefault()
      $(@).parent().hide 'slide', {direction: 'right'}, 'fast'

  if $('.slider-buttons').length
    $(window).load ->
      homePage()

homePage = ->
#    services slider
  $.fn.slideServices = ->
    $('#home-slides #slide-1 .slider-img, #home-slides #slide-1 .hero-unit').show 'slide',{direction: 'up'}, 1000
    autoService = ->
      window.setInterval (->
        if($('.slider-buttons .span3.active').next().attr('value'))
          $('.slider-buttons .span3.active').removeClass('active').next().addClass('active')
        else
          $('.slider-buttons .span3.active').removeClass('active')
          $('.slider-buttons .span3').first().addClass('active')
        $(".slider-buttons").trigger "cssClassChanged"

      ), 10000
    tabs = $(@).find('.span3')
    autos = autoService()
    $(@).bind 'cssClassChanged', ->
      activeTab = $(@).find('.span3.active')
      activeSlide = activeTab.attr('value')
      $("#home-slides .slides.active .hero-unit").hide('slide', {direction: 'left'})
      $("#home-slides .slides.active .slider-img").hide('slide', {direction: 'right'})
      $('#home-slides').removeClass().addClass activeSlide
      $("#home-slides .slides").removeClass "active"
      $("#home-slides #" + activeSlide).addClass "active", ->
        $("#home-slides #"+activeSlide+" .hero-unit").show('slide', {direction: 'left'}, 700)
        $("#home-slides #"+activeSlide+" .slider-img").show('slide', {direction: 'right'}, 700)

    tabs.on "click", ->
      return if $(this).hasClass 'active'
      tabs.removeClass "active"
      $(this).addClass "active"
      $(".slider-buttons").trigger "cssClassChanged"
      window.clearInterval(autos)
      autos = autoService()

#        reviews slider
  $.fn.slideReviews = ->
    $('#reviews #review-1').show('slide',{direction: 'up'}, 700)
    buttons = $(@).find('li')

    autoReview = ->
      window.setInterval (->
        if($('.review-buttons li.active').next().attr('value'))
          $('.review-buttons li.active').removeClass('active').next().addClass('active')
        else
          $('.review-buttons li.active').removeClass('active')
          $('.review-buttons li').first().addClass('active')
        $('.review-buttons').trigger 'reviewClassChanged'

      ), 10000

    autos = autoReview()

    $(@).bind 'reviewClassChanged', ->
      activeButton = $(@).find('li.active')
      activeReview = activeButton.attr('value')
      if document.documentElement.clientWidth > 800
        $('#reviews .review.active').stop().hide 'clip', ->
          $('#reviews .review').removeClass 'active'
          $('#reviews #' + activeReview).addClass 'active'
          $('#reviews #'+activeReview).stop().show 'clip'
      else
        $('#reviews .review.active').stop().hide()
        $('#reviews .review').removeClass 'active'
        $('#reviews #' + activeReview).addClass 'active'
        $('#reviews #'+activeReview).stop().show()

    buttons.on "click", ->
      return if $(this).hasClass 'active'
      buttons.removeClass "active"
      $(this).addClass 'active'
      $('.review-buttons').trigger 'reviewClassChanged'
      window.clearInterval(autos)
      autos = autoReview()

  #    Load sliders
  $('.slider-buttons').slideServices()
  $('.review-buttons').slideReviews()

#  if $('#plane1').length
#    plane1 = $('#plane1')
#    plane2 = $('#plane2')
#    left = plane1.position().left / plane1.parent().width() * 100
#    top = plane1.position().top / plane1.parent().height() * 100
#    left2 = plane2.position().left / plane2.parent().width() * 100
#    top2 = plane2.position().top / plane2.parent().height() * 100
#
#    nIntervId = setInterval (
#      ->
#        planeFlight plane1, top, left
#    ), 2500
#
#    nIntervId2 = setInterval (
#      ->
#        planeFlight2 plane2, top2, left2
#    ), 10000

#  if $('#home-slides').length
#    $('.slider-header').hide();
#    $('.slider-header').show('bounce', 500);
#    $('.slider-text').slideDown()
#    $('.slider-img').slideDown()


#planeFlight = (plane, top, left) ->
#  i = top.toFixed(1)
#  if left is not 0
#    j = left.toFixed(1)
#  else
#    j = 0
#  while i >= -40
#    i -= 0.1
#    j += 0.1
#    plane.animate(top: i+'%', 1)
#    plane.animate(left: j+'%', 1)
#
#planeFlight2 = (plane, top, left) ->
#  i = top.toFixed(1)
#  j = left.toFixed(1)
#  while i >= -40
#    i -= 0.1
#    j -= 0.1
#    plane.animate(top: i+'%', 1)
#    plane.animate(left: j+'%', 1)



#rand = (arr) ->
#  arr[Math.floor(Math.random() * arr.length)]