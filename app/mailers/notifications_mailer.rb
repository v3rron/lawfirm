class NotificationsMailer < ActionMailer::Base
  default :from => "noreply@abarskaya.com"
  default :to => "a.v.barskaya@gmail.com"

  def new_message(message)
    @message = message
    mail(:subject => "[abarskaya.com] #{message.subject}")
  end
end
