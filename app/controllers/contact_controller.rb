class ContactController < ApplicationController
  def new
    @message = Message.new
    render partial: 'new'
  end

  def create
    @message = Message.new(params[:message])

    if @message.valid?
      NotificationsMailer.new_message(@message).deliver
      redirect_to('/'+locale.to_s, :notice => t(:message_delivered))
    else
      redirect_to('/'+locale.to_s, :alert => t(:message_error))
    end
  end
end
